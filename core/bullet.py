from core.entity import Entity


class Bullet(Entity):

    def __init__(self,  x_coordinate, y_coordinate, speed_y, width, height, damage):
        """

        :param int x_coordinate: the x-position where the bullet is located
        :param int y_coordinate: fixed y-coordinate of the bullet
        :param int speed_y: the vertical distance the bullet can move per iteration
        :param int width: width of the bullet
        :param int height: height of the bullet
        :param int damage: how much damage it can do to a space invader
        """

        Entity.__init__(self, width=width, height=height, x_coordinate=x_coordinate, y_coordinate=y_coordinate)
        self.speed_y = speed_y
        self.damage = damage
        self.alive = True

    def out_of_screen(self):
        """
        Check if a bullet has reached the top of the screen

        :return: True if the bullet has reached the top
                    otherwise False
        """

        return self.y_coordinate + self.rectangle.height < 0

    def update_position(self):
        """
        Change the x-position with the speed value in the direction of movement. If the space invader gets to the edge
        it changes direction and moves down.
        """

        if not self.alive:
            return

        self._update_coordinates()
        self._update_rectangle()

    def _update_coordinates(self):
        # Speed upwards is positive, but because coordinates increase going down, subtract speed
        self.y_coordinate -= self.speed_y
