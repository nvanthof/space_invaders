import pygame

# Colours
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)


class UserInterface:

    def __init__(self, screen_width, screen_height):
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.screen = pygame.display.set_mode((screen_width, screen_height))

    def draw_screen(self, player, list_space_invaders, list_bullets):
        self.screen.fill(BLACK)

        pygame.draw.rect(self.screen, GREEN, player.rectangle)

        for space_invader in list_space_invaders:
            pygame.draw.rect(self.screen, RED, space_invader.rectangle)

        for bullet in list_bullets:
            pygame.draw.rect(self.screen, BLUE, bullet.rectangle)

        pygame.display.flip()

