import time

from core.entity import Entity
from core.bullet import Bullet


class Player(Entity):

    def __init__(self, x_coordinate, y_coordinate, speed, width, height, time_between_shooting):
        """
        Player class that is to be controlled by the user. Currently can only move left and right.

        :param int x_coordinate: the x-position where the player is located
        :param int y_coordinate: fixed y-coordinate of the player
        :param int speed: the horizontal distance the player can move per iteration
        :param int width: width of the player
        :param int height: height of the player
        :param float time_between_shooting: time in seconds between bullets shot
        """

        Entity.__init__(self, width=width, height=height, x_coordinate=x_coordinate, y_coordinate=y_coordinate)
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.speed = speed
        self.width = width
        self.height = height

        self.time_between_shooting = time_between_shooting
        self.time_last_bullet_shot = 0

    def update_position(self, direction, screen_width):
        """
        Move the player towards <direction> with distance <speed>

        :param int direction: either 1 (right) or -1 (left)
        :param int screen_width: the maximum x coordinate the player can reach
        """

        self._update_coordinates(direction, screen_width)
        self._update_rectangle()

    def _update_coordinates(self, direction, screen_width):
        self.x_coordinate += direction * self.speed
        self.x_coordinate = min(screen_width - self.width, max(0, self.x_coordinate))

    def shoot_bullet(self, bullet_settings):
        if time.time() - self.time_last_bullet_shot < self.time_between_shooting:
            return None

        self.time_last_bullet_shot = time.time()
        bullet = Bullet(x_coordinate=self.x_coordinate,
                        y_coordinate=self.y_coordinate,
                        speed_y=bullet_settings['speed_y'],
                        width=bullet_settings['width'],
                        height=bullet_settings['height'],
                        damage=bullet_settings['damage'])
        return bullet
