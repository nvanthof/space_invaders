import unittest

from core.bullet import Bullet


class TestBullet(unittest.TestCase):
    def setUp(self):
        self.bullet = Bullet(x_coordinate=10,
                             y_coordinate=30,
                             speed_y=10,
                             width=10,
                             height=20,
                             damage=10)

    def test_out_of_screen(self):
        """
        Check if the bullet is only considered out of screen when it's entirely out of screen
        """

        out_of_screens = [False, False, False, False, False, True, True, True]

        for out_of_screen in out_of_screens:
            self.bullet.update_position()
            self.assertEqual(out_of_screen, self.bullet.out_of_screen())

    def test_update_position(self):
        """
        Check if the y_coordinate is updated correctly
        """

        positions = [20, 10, 0, -10]

        for position in positions:
            self.bullet.update_position()
            self.assertEqual(position, self.bullet.y_coordinate)


if __name__ == '__main__':
    unittest.main()
