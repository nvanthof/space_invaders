import unittest

from core.space_invader import SpaceInvader


class TestSpaceInvader(unittest.TestCase):
    def setUp(self):
        self.space_invader = SpaceInvader(health=100,
                                          x_coordinate=0,
                                          y_coordinate=100,
                                          speed_x=10,
                                          y_incrementation=10,
                                          width=0,
                                          height=0,
                                          maximum_x_coordinate=50,
                                          maximum_y_coordinate=200)

    def test_update_position(self):
        """
        First tests if a single update works correctly.

        Then tests if movement across the screen works correctly (back and forth, as well as down)
        """

        # Check if moving once works
        self.space_invader.update_position()
        self.assertEqual(self.space_invader.x_coordinate, 10, msg='Standard update')
        self.assertEqual(self.space_invader.y_coordinate, 100, msg='Standard update')
        
        # Check if moving down and the other direction works
        correct_coordinates = [(20, 100), (30, 100), (40, 100), (50, 100),
                               (50, 110), (40, 110), (30, 110), (20, 110), (10, 110), (0, 110),
                               (0, 120), (10, 120)]
        for correct_x, correct_y in correct_coordinates:
            self.space_invader.update_position()
            self.assertEqual(self.space_invader.x_coordinate, correct_x, msg='Test move across and back')
            self.assertEqual(self.space_invader.y_coordinate, correct_y, msg='Test move across and back')

    def test_get_hit(self):
        """
        Test if the space invader correctly updates the health when getting hit
        """

        self.space_invader.get_hit(damage=10)
        self.assertEqual(self.space_invader.health, 90)

    def test_reached_bottom(self):
        """
        Test whether the space invader correctly identifies if it reached the bottom (y_coordinate <= 0)
        """

        list_reached_bottom = [False, False, False, False, False, False, False, False, False, True, True]
        self.space_invader.maximum_x_coordinate = 0

        for reached_bottom in list_reached_bottom:
            self.space_invader.update_position()
            self.assertEqual(self.space_invader.reached_bottom(), reached_bottom)


if __name__ == '__main__':
    unittest.main()
