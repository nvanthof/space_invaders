# Space_invaders

This is an example project with the goal to illustrate correct programming 
practices.

To run the game, first copy config/config.yaml-template to config/config.yaml,
no changes have to be made. The only reason it has not been added is because
other project could have passwords stored in here.

Be sure to install all requirements from the requiremets.txt .

The game is controlled from the core/game_mechanics.py which cycles through
all the steps and draws the screen after which it sleep 1/23 second. This is
simply to make it a relatively consistent game. However, it will slow down if 
background processes are running. This was a design decision.

To play the game, run main.py. Move the player using the arrow keys and shoot
using space. Space invaders need to be hit twice to kill.